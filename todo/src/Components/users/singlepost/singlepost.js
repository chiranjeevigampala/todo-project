import {Component} from "react";

class Singlepost extends Component{

    constructor(props){
        super(props);
        // this.handleDelete = this.handleDelete.bind(this);
        this.state = {
            posts: this.props.items
        }
    }

    // handleDelete(id){
    //  let index =  this.props.items.findIndex((post)=>{
    //        if(post.id === id){
    //            return true;
    //        }
    //    })
    //    let arr = [...this.state.posts];
    //    arr.splice(index,1);
    //    this.setState({
    //        posts: arr
    //    })
    //    debugger
    // }

    render() {
        
        return (
            <div>
                {
                this.props.items.map((post,index)=>{
                    return(
                    <div key={post.id}  id ={post.id}>
                        <div>{post.text}</div>
                        <input type = "text" value={this.props.text} onChange = {(event) => this.props.handleEdit(post.id,event)} />
                        <button onClick ={() => this.props.handleDelete(post.id)}>Delete</button>
                    </div>
                    )
                })
                }
            </div>
        )
    }
}

export default Singlepost;