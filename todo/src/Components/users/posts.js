import {Component} from "react";
import Singlepost from "./singlepost/singlepost";

class Posts extends Component{

    constructor(props){
        super(props);
        this.state = {
             items:[],
             text: '',
        }
        this.handleData = this.handleData.bind(this);
        this.inputHandler = this.inputHandler.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }

    handleDelete(id){
        let index =  this.state.items.findIndex((post)=>{
              if(post.id === id){
                  return true;
              }
          })
          this.state.items.splice(index,1);
          this.setState({
                  posts: this.state.items
              });
       }
       
       handleEdit(id,e){
        let index =  this.state.items.findIndex((post)=>{
            if(post.id === id){
                return true;
            }
        })
       let arr =  this.state.items;
       arr[index].text = e.target.value;
        this.setState({
                posts: arr
            });
       }

    handleData(e){
        e.preventDefault();
        if (this.state.text.length === 0) {
            return;
          }
          const newItem = {
            text: this.state.text,
            id: Date.now()
          };
          this.setState(state => ({
            items: state.items.concat(newItem),
            text: ''
          }));
        console.log(this.state)
    }

    inputHandler(e){
        this.setState({
            text:e.target.value
        })
    }

    render(){
        return(
            <div>
                <form onSubmit = {this.handleData}>
                <input type="text" value = {this.state.text} onChange = {this.inputHandler} />
                <button>Add Data</button>
                </form>
                <Singlepost handleEdit={this.handleEdit} handleDelete = {this.handleDelete} items = {this.state.items}/>
            </div>
        )
    }
}

export default Posts;